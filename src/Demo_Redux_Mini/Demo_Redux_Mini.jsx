import React, { Component } from "react";
import { connect } from "react-redux";
import {
  tangnumber,
  giamnumber,
} from "../Demo_Redux_Mini/redux/constants/numberConstant";
import { giamsoluongAction, tangsoluongAction } from "./redux/actions/numberAction";

class Demo_Redux_Mini extends Component {
  render() {
    return (
      <div>
        <h1 style={{ color: "red" }}>Demo Redux NUMBER</h1>
        <h3 style={{ color: "Purple" }} className="m-4">
          {this.props.soLuong}
        </h3>
        <button
          className="btn btn-outline-danger m-4"
          onClick={this.props.giamSoLuong}
        >
          -
        </button>

        <button
          className="btn btn-outline-warning"
          onClick={this.props.tangSoLuong}
        >
          +
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    soLuong: state.numberReducer.number,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    tangSoLuong: () => {
      dispatch (tangsoluongAction());
    },
    giamSoLuong: () => {
      dispatch(giamsoluongAction());
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Demo_Redux_Mini);
