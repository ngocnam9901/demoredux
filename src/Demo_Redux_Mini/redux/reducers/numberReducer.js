import {
  tangnumber,
  giamnumber,
} from "../constants/numberConstant";

let initialState = {
  number: 0,
};

export const numberReducer = (state = initialState, action) => {
  switch (action.type) {

    case tangnumber : {
      if (state.number == 10) {
        state.number = 0;
      } else {
        state.number += action.payload;
      }

      return { ...state };
    }
    case giamnumber : {
      if (state.number == -10) {
        state.number = 0;
      } else {
        state.number -= action.payload;
      }

      return { ...state };
    }
    default:
      return state;
  }
};
