import { tangnumber, giamnumber } from "../constants/numberConstant";

export const tangsoluongAction = () => {
  return {
    type: tangnumber,
    payload: 1,
  };
};
export const giamsoluongAction = () => {
  return {
    type: giamnumber,
    payload: 1,
  };
};
