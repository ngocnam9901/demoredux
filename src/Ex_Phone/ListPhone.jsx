import React, { Component } from "react";
import ItemPhone from "./ItemPhone";

export default class ListPhone extends Component {
  renderContent = () => {
    return this.props.data.map((phone) => {
      return (
        <ItemPhone handleChangeDetail={this.props.hanleClick} phone={phone} />
      );
    });
  };
  render() {
    return <div className="row">{this.renderContent()}</div>;
  }
}
