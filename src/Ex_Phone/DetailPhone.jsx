import React, { Component } from "react";

export default class DetailPhone extends Component {
  render() {
    return (
      <div className="row mt-5 p-5 border border-danger">
        <div className="col-3">
          <img src={this.props.detail.hinhAnh} className="w-100" alt="" />
        </div>

        <div className="col-9">
          <p> {this.props.detail.tenSP}</p>
          <p> {this.props.detail.giaBan}</p>
          <p> {this.props.detail.manHinh}</p>
        </div>
      </div>
    );
  }
}
