import React, { Component } from "react";

export default class ItemPhone extends Component {
  render() {
    let { phone } = this.props;
    return (
      <div className="col-4 ">
        <div className="card text-left h-100">
          <img className=" card-img-top" src={phone.hinhAnh} alt />
          <div className="card-body">
            <h4 className="card-title">{phone.tenSP}</h4>
            <p className="card-text ">
              <button
                onClick={() => {
                  this.props.handleChangeDetail(phone);
                }}
                className="btn btn-warning"
              >
                Xem chi tiết
              </button>
            </p>
          </div>
        </div>
      </div>
    );
  }
}
var a = 4;

var b = a;

var c = b;
