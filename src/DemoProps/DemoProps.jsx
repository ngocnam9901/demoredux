import React, { Component } from "react";
import UserInfor from "./UserInfor";

export default class DemoProps extends Component {
  state = {
    name: "alice",
    age: 2,
  };

  handleChangeName = () => {
    this.setState({
      name: "Bob",
    });
  };
  render() {
    return (
      <div>
        <p>Demo props</p>
        <button onClick={this.handleChangeName} className="btn btn-success">
          Change name to Bob
        </button>
        <UserInfor
          handleClick={this.handleChangeName}
          user={this.state}
          isLogin={true}
          title="Title Alice"
        />

        <UserInfor
          handleClick={this.handleChangeName}
          user={this.state}
          isLogin={true}
          title="Title Bob"
        />
      </div>
    );
  }
}
