import React, { Component } from "react";

export default class DataBinding extends Component {
  userAge = 2;
  renderFooter = () => {
    return (
      <>
        <h1>Footer</h1>
        <h1>Footer</h1>
        <h1>Footer</h1>
      </>
    );
  };
  render() {
    let username = "Bob";

    let imgSrc =
      "https://static.mservice.io/blogscontents/momo-upload-api-200316163115-637199730753785441.jpg";
    return (
      <div>
        <div>Hello {username}</div>
        <p>Userage : {this.userAge}</p>
        <img
          style={{
            width: "300px",
            marginTop: 100,
          }}
          src={imgSrc}
          alt=""
        />
        {this.renderFooter()}
        {this.renderFooter()}
      </div>
    );
  }
}
